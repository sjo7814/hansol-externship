package com.example.hansolexternship.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseMessage {

    public static final String RESULT_NOT_FOUND = "조회 결과가 없습니다.";
    public static final String INSERTED = "저장되었습니다.";
    public static final String UPDATED = "수정되었습니다.";
    public static final String DELETED = "삭제되었습니다.";

    public static ResponseEntity<BasicResponse> getCommonResponseEntity(HttpStatus httpStatus, String message){
        CommonResponse response = CommonResponse.builder()
                .httpStatus(httpStatus)
                .message(message)
                .build();
        return new ResponseEntity<>(response, httpStatus);
    }

    public static ResponseEntity<BasicResponse> getDataResponseEntity(HttpStatus httpStatus, Object data){
        DataResponse<Object> response = DataResponse.builder()
                .httpStatus(httpStatus)
                .data(data)
                .build();
        return new ResponseEntity<>(response, httpStatus);
    }

    public static ResponseEntity<BasicResponse> getErrorResponseEntity(HttpStatus httpStatus, String message){
        ErrorResponse response = ErrorResponse.builder()
                .httpStatus(httpStatus)
                .message(message)
                .build();
        return new ResponseEntity<>(response, httpStatus);
    }
}
