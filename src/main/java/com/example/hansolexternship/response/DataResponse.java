package com.example.hansolexternship.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
public class DataResponse<T> extends BasicResponse{

    @ApiModelProperty(example = "HTTP 응답 상태 코드")
    private final int code;

    @ApiModelProperty(example = "HTTP 응답 상태")
    private final String status;

    @ApiModelProperty(example = "응답 데이터")
    private final T data;

    @Builder
    public DataResponse(HttpStatus httpStatus, T data){
        this.code = httpStatus.value();
        this.status = httpStatus.getReasonPhrase();
        this.data = data;
    }

}
