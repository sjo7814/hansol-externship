package com.example.hansolexternship.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ErrorResponse extends BasicResponse {

    @ApiModelProperty(example = "HTTP 응답 상태 코드")
    private final int errorCode;

    @ApiModelProperty(example = "HTTP 응답 상태")
    private final String status;

    @ApiModelProperty(example = "에러 메시지")
    private final String message;

    @Builder
    public ErrorResponse(HttpStatus httpStatus, String message){
        this.errorCode = httpStatus.value();
        this.status = httpStatus.getReasonPhrase();
        this.message = message;
    }
}
