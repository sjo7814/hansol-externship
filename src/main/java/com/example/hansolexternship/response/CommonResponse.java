package com.example.hansolexternship.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class CommonResponse extends BasicResponse{

    @ApiModelProperty(example = "HTTP 응답 상태 코드")
    private final int code;

    @ApiModelProperty(example = "HTTP 응답 상태")
    private final String status;

    @ApiModelProperty(example = "응답 메시지")
    private final String message;

    @Builder
    public CommonResponse(HttpStatus httpStatus, String message){
        this.code = httpStatus.value();
        this.status = httpStatus.getReasonPhrase();
        this.message = message;
    }

}
