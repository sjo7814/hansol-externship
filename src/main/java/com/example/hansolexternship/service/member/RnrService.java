package com.example.hansolexternship.service.member;

import com.example.hansolexternship.dto.Rnr;

import java.util.List;

public interface RnrService {

    List<Rnr> findAll();

    Rnr findById(Long id);

    int insert(Rnr rnr);

    int update(Rnr rnr, Long rnrId);

    int delete(Long id);

}
