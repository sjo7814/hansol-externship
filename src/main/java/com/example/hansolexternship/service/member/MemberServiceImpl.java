package com.example.hansolexternship.service.member;

import com.example.hansolexternship.dto.Member;
import com.example.hansolexternship.mapper.MemberMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService{

    private final MemberMapper mapper;

    @Override
    public List<Member> findAll() {
        return mapper.findAll();
    }

    @Override
    public Member findById(Long id) {
        return mapper.findById(id);
    }

    @Override
    public Member findByName(String name) {
        return mapper.findByName(name);
    }

    @Override
    public Member findByGrade(String grade) {
        return mapper.findByGrade(grade);
    }

    @Override
    public int insert(Member member) {
        return mapper.insert(member);
    }

    @Override
    public int update(Member member, Long memberId) {
        return mapper.update(member, memberId);
    }

    @Override
    public int delete(Long id) {
        return mapper.delete(id);
    }
}
