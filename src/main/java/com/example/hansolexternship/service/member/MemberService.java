package com.example.hansolexternship.service.member;

import com.example.hansolexternship.dto.Member;

import java.util.List;

public interface MemberService {

    List<Member> findAll();

    Member findById(Long id);

    Member findByName(String name);

    Member findByGrade(String grade);

    int insert(Member member);

    int update(Member member, Long memberId);

    int delete(Long id);

}
