package com.example.hansolexternship.service.member;

import com.example.hansolexternship.dto.Rnr;
import com.example.hansolexternship.mapper.RnrMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class RnrServiceImpl implements RnrService{

    private final RnrMapper mapper;

    @Override
    public List<Rnr> findAll() {
        return mapper.findAll();
    }

    @Override
    public Rnr findById(Long id) {
        return mapper.findById(id);
    }

    @Override
    public int insert(Rnr rnr) {
        int result = mapper.insertRnr(rnr);
        if(result != 0){
            mapper.insertCompanyList(rnr.getId(), rnr.getCompany().split("/"));
        }
        return result;
    }

    @Override
    public int update(Rnr rnr, Long rnrId) {
        return mapper.update(rnr, rnrId, rnr.getCompany().split("/"));
    }

    @Override
    public int delete(Long id) {
        return mapper.delete(id);
    }

}
