package com.example.hansolexternship.mapper;

import com.example.hansolexternship.dto.Rnr;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RnrMapper {

    List<Rnr> findAll();

    Rnr findById(Long id);

    int insertRnr(@Param("rnr") Rnr rnr);

    int insertCompanyList(Long rnrId, String[] companyList);

    int update(Rnr rnr, Long rnrId, String[] companyList);

    int delete(Long id);

}
