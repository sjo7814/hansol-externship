package com.example.hansolexternship.mapper;

import com.example.hansolexternship.dto.Member;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface MemberMapper {

    List<Member> findAll();

    Member findById(Long id);

    Member findByName(String name);

    Member findByGrade(String grade);

    int insert(Member member);

    int update(Member member, Long memberId);

    int delete(Long id);

}
