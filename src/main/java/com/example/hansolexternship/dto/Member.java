package com.example.hansolexternship.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Member {

    @ApiModelProperty(example = "담당자 ID")
    private Long id;

    @ApiModelProperty(example = "담당자 이름")
    private String name;

    @ApiModelProperty(example = "담당자 직급")
    private String grade;

    @ApiModelProperty(example = "담당자 전화번호")
    private String phone;


    @Builder
    public Member(Long id, String name, String grade, String phone) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.phone = phone;
    }

    @Builder
    public Member(String name, String grade, String phone) {
        this.name = name;
        this.grade = grade;
        this.phone = phone;
    }
}
