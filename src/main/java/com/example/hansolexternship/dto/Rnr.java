package com.example.hansolexternship.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Data
@NoArgsConstructor
public class Rnr {

    @ApiModelProperty(example = "R&R ID")
    private Long id;

    @ApiModelProperty(example = "업무 코드")
    private String roleCode;

    @ApiModelProperty(example = "업무 이름")
    private String roleName;

    @ApiModelProperty(example = "담당 회사")
    private String company;

    @ApiModelProperty(example = "담당자 ID")
    private Long memberId;

    @ApiModelProperty(example = "담당자 이름")
    private String memberName;

    @ApiModelProperty(example = "담당자 직급")
    private String memberGrade;

    @ApiModelProperty(example = "세부 업무")
    private String task;

    @ApiModelProperty(example = "담당자 전화번호")
    private String memberPhone;

    @Builder
    public Rnr(Long id, String roleCode, String roleName, String company, Long memberId, String memberName, String memberGrade, String task, String memberPhone) {
        this.id = id;
        this.roleCode = roleCode;
        this.roleName = roleName;
        this.company = company;
        this.memberId = memberId;
        this.memberName = memberName;
        this.memberGrade = memberGrade;
        this.task = task;
        this.memberPhone = memberPhone;
    }

    @Builder
    public Rnr(String roleCode, String roleName, String company, Long memberId, String memberName, String memberGrade, String task, String memberPhone) {
        this.roleCode = roleCode;
        this.roleName = roleName;
        this.company = company;
        this.memberId = memberId;
        this.memberName = memberName;
        this.memberGrade = memberGrade;
        this.task = task;
        this.memberPhone = memberPhone;
    }
}
