package com.example.hansolexternship.controller;

import com.example.hansolexternship.dto.Rnr;
import com.example.hansolexternship.response.BasicResponse;
import com.example.hansolexternship.response.ResponseMessage;
import com.example.hansolexternship.service.member.RnrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping( "/rnr")
@Api(tags = "R&R 관리 API")
public class RnrController {

    private final RnrService rnrService;


    @GetMapping
    @ApiOperation(value = "R&R 목록 조회", notes = "전체 R&R 목록을 조회합니다.")
    public ResponseEntity<BasicResponse> rnrList(){
        List<Rnr> result = rnrService.findAll();
        if(result == null || result.size() == 0){
            return ResponseMessage.getCommonResponseEntity(HttpStatus.OK, ResponseMessage.RESULT_NOT_FOUND);
        }
        return ResponseMessage.getDataResponseEntity(HttpStatus.OK, result);
    }
    
    @GetMapping("/{rnrId}")
    @ApiOperation(value = "ID로 R&R 조회", notes = "특정 ID를 가진 R&R을 조회합니다.")
    public ResponseEntity<BasicResponse> rnr(@PathVariable Long rnrId){
        Rnr result = rnrService.findById(rnrId);
        if(result == null){
            return ResponseMessage.getCommonResponseEntity(HttpStatus.OK, ResponseMessage.RESULT_NOT_FOUND);
        }
        return ResponseMessage.getDataResponseEntity(HttpStatus.OK, result);
    }

    @PostMapping("/new")
    @ApiOperation(value = "R&R 추가", notes = "R&R 목록에 새 R&R을 추가합니다.")
    public ResponseEntity<BasicResponse> newRnr(@RequestBody Rnr rnr){
        rnrService.insert(rnr);
        return ResponseMessage.getCommonResponseEntity(HttpStatus.CREATED, ResponseMessage.INSERTED);
    }

    @PutMapping("/{rnrId}")
    @ApiOperation(value = "R&R 정보 수정", notes = "특정 ID를 가진 R&R 정보를 수정합니다.")
    public ResponseEntity<BasicResponse> updateRnr(@PathVariable Long rnrId, @RequestBody Rnr rnr){
        rnrService.update(rnr,rnrId);
        return ResponseMessage.getCommonResponseEntity(HttpStatus.OK, ResponseMessage.UPDATED);
    }

    @DeleteMapping("/{rnrId}")
    @ApiOperation(value = "R&R 정보 삭제", notes = "특정 ID를 가진 R&R 정보를 목록에서 삭제합니다.")
    public ResponseEntity<BasicResponse> deleteRnr(@PathVariable Long rnrId){
        rnrService.delete(rnrId);
        return ResponseMessage.getCommonResponseEntity(HttpStatus.OK, ResponseMessage.DELETED);
    }
}
