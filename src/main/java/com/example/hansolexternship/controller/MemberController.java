package com.example.hansolexternship.controller;

import com.example.hansolexternship.dto.Member;
import com.example.hansolexternship.response.BasicResponse;
import com.example.hansolexternship.response.ResponseMessage;
import com.example.hansolexternship.service.member.MemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/member")
@RequiredArgsConstructor
@Api(tags = "담당자 관리 API")
public class MemberController {

    private final MemberService memberService;

    @GetMapping
    @ApiOperation(value = "담당자 목록 조회", notes = "전체 담당자 목록을 조회합니다.")
    public ResponseEntity<BasicResponse> memberList() {
        List<Member> result = memberService.findAll();
        if(result == null || result.size() == 0){
            return ResponseMessage.getCommonResponseEntity(HttpStatus.OK, ResponseMessage.RESULT_NOT_FOUND);
        }
        return ResponseMessage.getDataResponseEntity(HttpStatus.OK, result);
    }

    @GetMapping("/{memberId}")
    @ApiOperation(value = "ID로 담당자 조회", notes = "특정 ID를 가진 담당자를 조회합니다.")
    public ResponseEntity<BasicResponse> member(@PathVariable Long memberId){
        Member result = memberService.findById(memberId);
        if(result == null){
            return ResponseMessage.getCommonResponseEntity(HttpStatus.OK, ResponseMessage.RESULT_NOT_FOUND);
        }
        return ResponseMessage.getDataResponseEntity(HttpStatus.OK, result);
    }

    @PostMapping("/new")
    @ApiOperation(value = "담당자 추가", notes = "담당자 목록에 새 담당자를 추가합니다.")
    public ResponseEntity<BasicResponse> newMember(@RequestBody Member member){
        int result = memberService.insert(member);
        return ResponseMessage.getCommonResponseEntity(HttpStatus.CREATED, ResponseMessage.INSERTED);
    }

    @PutMapping("/{memberId}")
    @ApiOperation(value = "담당자 정보 수정", notes = "특정 ID를 가진 담당자의 정보를 수정합니다.")
    public ResponseEntity<BasicResponse> updateMember(@RequestBody Member member, @PathVariable Long memberId){
        int result = memberService.update(member, memberId);
        return ResponseMessage.getCommonResponseEntity(HttpStatus.OK, ResponseMessage.UPDATED);
    }

    @DeleteMapping("/{memberId}")
    @ApiOperation(value = "담당자 정보 삭제", notes = "특정 ID를 가진 담당자의 정보를 목록에서 삭제합니다.")
    public ResponseEntity<BasicResponse> deleteMember(@PathVariable Long memberId){
        int result = memberService.delete(memberId);
        return ResponseMessage.getCommonResponseEntity(HttpStatus.OK, ResponseMessage.DELETED);
    }


}
