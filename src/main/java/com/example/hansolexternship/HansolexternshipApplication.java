package com.example.hansolexternship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class HansolexternshipApplication {

    public static void main(String[] args) {
        SpringApplication.run(HansolexternshipApplication.class, args);
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("R&R 관리 API")
                .version("v1")
                .description("R&R 정보 관리를 위한 API입니다.")
                .build();
    }

    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.hansolexternship.controller"))
                .paths(PathSelectors.any())
                .build();
    }

}
